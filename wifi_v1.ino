#include "ESP8266.h"

#define SSID        "GVT-5DA0"
#define PASSWORD    "13061994"

SoftwareSerial WiFiSerial(2 , 3);

ESP8266 wifi(WiFiSerial);

void setup(void)
{
  Serial.begin(9600);
  force_setup();
}

void loop(void)
{
  uint8_t buffer[128] = {0};
  uint8_t mux_id;
  uint32_t len = wifi.recv(&mux_id, buffer, sizeof(buffer), 100);
  if (len > 0) {
    Serial.print("Status:[");
    Serial.print(wifi.getIPStatus().c_str());
    Serial.println("]");

    Serial.println("Received New Request:");
    for (uint32_t i = 0; i < len; i++) {
      Serial.print((char)buffer[i]);
    }
    
    char *response = "HTTP/1.1 201 OK\r\nContent-Type: text/json\r\n\r\n{\"status\": \"Opened\"}\r\n";
    if (wifi.send(mux_id, (const uint8_t*)response, strlen(response))) {
      Serial.println("Response Sent!");
    } else {
      Serial.println("Error! Response Not Sent!");
    }

    Serial.print("Releasing TCP");

    if (wifi.releaseTCP(mux_id)) {
      Serial.print("Release TCP ");
      Serial.print(mux_id);
      Serial.println(" ok");
      Serial.print("Status:[");
      Serial.print(wifi.getIPStatus().c_str());
      Serial.println("]");

    } else {
      Serial.print("Release TCP");
      Serial.print(mux_id);
      Serial.println(" Error");
      Serial.println("Forcing Setup...");
      force_setup();
    }
  }


}

void force_setup() {
  Serial.println("Setup Begin");

  if (wifi.setOprToStationSoftAP()) {
    Serial.print("Operations Ok\r\n");
  } else {
    Serial.print("Operations Error\r\n");
  }

  if (wifi.joinAP(SSID, PASSWORD)) {
    Serial.println("Join WiFi Success");
    Serial.print("IP: ");
    Serial.println(wifi.getLocalIP().c_str());
  } else {
    Serial.println("Join WiFi Failure");
  }

  if (wifi.enableMUX()) {
    Serial.print("Multiple Ok\r\n");
  } else {
    Serial.print("Multiple Err\r\n");
  }

  if (wifi.startTCPServer(8090)) {
    Serial.println("TCP Server Ok");
  } else {
    Serial.println("TCP Server Error");
  }

  if (wifi.setTCPServerTimeout(3)) {
    Serial.println("Timeout Set 3 Minutes");
  } else {
    Serial.println("TCP Set Timeout Error");
  }

  Serial.println("Setup End");
  Serial.println("Waiting For HTTP Request...");
}

